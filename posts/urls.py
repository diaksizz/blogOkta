from django.urls import path 


from .views import blog,post,category,tagged

urlpatterns = [
    path('',blog,name='blog'),
    path('post/<str:slug>/',post,name='posts'),
    path('category/<str:name>/',category,name='category'),
    path('tag/<slug:slug>/',tagged,name='tagged'),
    
]
