from django.shortcuts import render,redirect, get_object_or_404
from django.contrib.auth.decorators import login_required

# Create your views here.
from .models import Posts,Category
from .forms import PostForm, CommentForm
from taggit.models import Tag



def blog(request):
    posts = Posts.objects.all().order_by('-published')
    cats = Category.objects.all()
    common_tags = Posts.tags.most_common()

    return render(request,'posts/blog.html',{'posts':posts,'cats':cats,'common_tags':common_tags,})


def post(request,slug):
    posts = Posts.objects.filter(slug=slug)
    common_tags = Posts.tags.most_common()
    # data = get_object_or_404(Posts, pk=post_id)
    post = get_object_or_404(Posts, slug=slug)
    cats = Category.objects.all()
    comments = post.comments.filter(active=True)
    new_comment = None
    # Comment posted
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
 
            # Create Comment object but don't save to database yet
            new_comment = comment_form.save(commit=False)
            # Assign the current post to the comment
            new_comment.post = post
            # Save the comment to the database
            new_comment.save()
    else:
        comment_form = CommentForm()
    
    return render(request,'posts/post.html',{'posts':posts,'cats':cats,'common_tags':common_tags,'post': post,'comments': comments,'new_comment': new_comment,'comment_form': comment_form,})


def category(request,name):   
    cats = Category.objects.all() 
    posts =  Posts.objects.filter(category__name=name)
    common_tags = Posts.tags.most_common()
    
   
    return render(request,'posts/blog.html',{'posts':posts,'cat':name,'common_tags':common_tags,'cats':cats,})


def categories(request):
    cats = Category.objects.all()
    
    
    return render(request,'widget.html',{'cats':cats,})

def tagged(request,slug):
    tag = get_object_or_404(Tag, slug=slug)
    posts = Posts.objects.filter(tags=tag)
    cats = Category.objects.all()
    common_tags = Posts.tags.most_common()
    context = {
        'tag':tag,
        'posts':posts,
        'common_tags':common_tags,
        'cats':cats,
    }
    return render(request,'detil.html',context,)

